/* eslint-disable linebreak-style */


const jsforce = require('jsforce');
const axios = require('axios');
const sql = require('mssql');
const SalesforceMiddleware = require('../Salesforce/SalesforceMiddleware');

exports.SQLQuery = async (query, callback, domainList) => {
  // HARD CODED THE CONFIG TO BE OVAL FOR TESTING AS PER C# JUST SWITCH OVER.
  console.log(query, 'QUERY');
  const ovalConfig = {
    server: '10.210.2.6',
    // If you're on Windows Azure, you will need this:
    options: { encrypt: true },
    authentication: {
      type: 'default',
      options: {
        userName: 'Luke',
        password: '12Zybvws',
      },
    },
  };

  try {
    await sql.connect(ovalConfig);
    await sql.query(query, (err, result) => {
      callback(result);
    });
  } catch (err) {
    console.log(err);
  }
};

exports.insertIntoSQL = (response, company) => new Promise((resolve, reject) => {
  const ovalConfig = {
    server: '10.210.2.6',
    // If you're on Windows Azure, you will need this:
    options: { encrypt: true },
    authentication: {
      type: 'default',
      options: {
        userName: 'Luke',
        password: '12Zybvws',
      },
    },
  };

  sql.connect(ovalConfig)
    .then(() => {
      const request = new sql.Request()
        .input('json', sql.VarChar, JSON.stringify(response.data))
        .input('clientid', sql.VarChar, 'Lusha')
        .input('originalID', sql.Int, company.Id)
        .query('INSERT INTO Staging.dbo.EnrichmentAPIResults (OriginalID, rawJSON, ClientID) VALUES(@originalID, @json, @clientID)')
        .then((done) => {
          sql.close();
          resolve();
        });
    });
});

// DEPRICATED
// // const getTestData = () => {
// //   const ovalConfig = {
// //     server: '10.210.2.6',
// //     // If you're on Windows Azure, you will need this:
// //     options: { encrypt: true },
// //     authentication: {
// //       type: 'default',
// //       options: {
// //         userName: 'Luke',
// //         password: '12Zybvws',
// //       },
// //     },
// //   };


// //   return a = sql.connect(ovalConfig)
// //     .then(() => request = new sql.Request()
// //       .query('SELECT TOP(2) * FROM Staging.dbo.dataenrichment_company')
// //       .then((companies) => {
// //         sql.close();
// //         return companies.recordset;
// //       }));
// // };
