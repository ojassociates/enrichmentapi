/* eslint-disable linebreak-style */

exports.enrichCompanies = (originalCompanies, enrichedCompanies) => new Promise((resolve, reject) => {
  console.log('HEYO', originalCompanies, '\n', enrichedCompanies);
  originalCompanies.map((company, mapIndex) => {
    console.log('ORIGINAL COMPANIES MAP');
    for (let index = 0; index < enrichedCompanies.length; index++) {
      console.log(`DOMAINS: ${enrichedCompanies[index].Domain__c} | ${company.Domain__c}`);
      if (enrichedCompanies[index].Domain__c === company.Domain__c) {
        console.log(`MATCHED: ${enrichedCompanies[index].Domain__c} AND ${company.Domain__c}`);
        const companyCopy = company;
        companyCopy.No_of_Employees__c = enrichedCompanies[index].No_of_Employees__c;
        companyCopy.Date_Founded__c = enrichedCompanies[index].Date_Founded__c;
        enrichedCompanies.push(companyCopy);
        console.log(enrichedCompanies);
      }
      console.log(`Original Companies Length: ${originalCompanies.length} | CURRENT INDEX: ${mapIndex}`);
      if (originalCompanies.length === mapIndex) {
        console.log('BANG');
        resolve(enrichedCompanies);
      }
    }
  });
});
