/* eslint-disable linebreak-style */
const sql = require('mssql');
const jsforce = require('jsforce');

const conn = new jsforce.Connection();


const loginToSalesforce = () => new Promise((resolve, reject) => {
  const instanceUrl = 'https://test.salesforce.com';

  const accessToken = '6Cel800D0C0000000Ydx8880C00000091TRVWJNCc1BT9hUUNMWiyDFwgNWcl0Ew32w7jvJRRUI4LEmF2vpvJUp2RpOzB2yAlmiiZGmd0Zc';

  const userName = 'marcus.baxendell@ojassociates.com.platform3';

  const password = 'Password1!';

  const conn = new jsforce.Connection({
    loginUrl: instanceUrl,
  });

  conn.login(userName, password,
    (err, userInfo) => {
      if (err) {

        // return console.error(err);
      }
      resolve(conn);
    });
});

exports.getSalesforceFilteredData = (enrichedCompanies, callback) => {
  // Salesforce Data FROM TANK.
  let enrichedProfileString;
  const ovalConfig = {
    server: '10.210.2.6',
    // If you're on Windows Azure, you will need this:
    options: { encrypt: true },
    authentication: {
      type: 'default',
      options: {
        userName: 'Luke',
        password: '12Zybvws',
      },
    },
  };

  enrichedCompanies.map((profile, index) => {
    const domain = profile.urls.homepage.replace('www.', '').replace('https://', '').replace('http://', '');
    if (index === 0) {
      enrichedProfileString = `${domain}`;
    } else {
      enrichedProfileString = `${enrichedProfileString},${domain}`;
    }
  });
  sql.connect(ovalConfig)
    .then((con, err) => {
      const request = new sql.Request()
        .input('DomainList', sql.VarChar, enrichedProfileString)
        .execute('CDR_OJSalesforce_Sandbox_POC.dbo.DomainToCompany_EnrichmentProfile')
        .then((res) => {
          console.log(res, 'RESPONSE FROM SPROC.');
          sql.close();
        });
    });
};


const buildSalesforceModel = function (account) {
  const company = {};

  Object.keys(account).map((key) => {
    company[key] = '';
  });


  const c = class Company {
    constructor() {
      Object.assign(this, company);
    }
  };


  return c;
};


exports.getSalesforceCompanyModel = (type = 'default') => new Promise((resolve, reject) => {
  loginToSalesforce()
    .then((conn) => {
      conn.sobject('Account').retrieve('0010C000005YUK2QAO', (err, account) => {
        if (err) { return console.error(err); }
        const SalesforceCompany = buildSalesforceModel(account);
        resolve(SalesforceCompany);
      })
        .catch((err) => {
          resolve(err);
          res.send(err, 'DIDN\'T CONNECT');
        });
    });
});
