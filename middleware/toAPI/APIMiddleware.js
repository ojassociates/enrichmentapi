/* eslint-disable linebreak-style */

const sql = require('mssql');
const axios = require('axios');

const SalesforceMiddleware = require('../Salesforce/SalesforceMiddleware');

exports.callCompany = (company) => {
  sql.close();
  const ovalConfig = {
    server: '10.210.2.6',
    // If you're on Windows Azure, you will need this:
    options: { encrypt: true },
    authentication: {
      type: 'default',
      options: {
        userName: 'Luke',
        password: '12Zybvws',
      },
    },
  };
  const headers = { headers: { api_key: 'b196d179-2ecf-4d22-a77c-7d1dcbec1b2d' } };
  console.log('STARTED CALLING COMPANY');
  sql.connect(ovalConfig)
    .then(() => {
      axios.get(`https://api.lusha.co/company?domain=${company.ClientWebsite.replace('www.', '').replace('http://')}`, headers, { timeout: 2 })
        .then((response) => {
          const apiResponse = response.data;
          const request = new sql.Request()
            .input('json', sql.VarChar, JSON.stringify(apiResponse))
            .input('clientid', sql.VarChar, 'Lusha')
            .input('originalID', sql.Int, company.Id)
            .query('INSERT INTO Staging.dbo.EnrichmentAPIResults (OriginalID, rawJSON, ClientID) VALUES(@originalID, @json, @clientID)')
            .then((done) => {
              console.log(`DONE ${company.ClientWebsite}`);
            });
        });
    });
};


exports.getCompanyData = (company) => new Promise((resolve, reject) => {
  console.log('Hit');
  const headers = { headers: { api_key: 'b196d179-2ecf-4d22-a77c-7d1dcbec1b2d' } };
  axios.get(`https://api.lusha.co/company?domain=${company.ClientWebsite.replace('www.', '').replace('http://')}`, headers, { timeout: 2 })
    .then((response) => {
      sql.close();
      console.log(`ADDED ${company.ClientWebsite}`);
      resolve(response, company);
    })
    .catch((err) => {
      reject();
    });
});


exports.getUpdatedEnrichmentData = (enrichmentEndpoint, headers, type = 'default') => new Promise((resolve, reject) => {
  const dateNow = new Date();
  const weekBefore = new Date(dateNow.getTime() - (60 * 60 * 24 * 7 * 1000));
  const queryTime = `?startDate=${weekBefore.toISOString()}&endDate=${dateNow.toISOString()}`;
  const mappedCompanies = [];


  axios.get(enrichmentEndpoint + queryTime, headers)
    .then((res) => {
      res.data.data.map((company, index) => {
        SalesforceMiddleware.getSalesforceCompanyModel(company)
          .then((SalesforceModel) => {
            const enrichedProfile = new SalesforceModel(company);

            if (type === 'talentticker') {
              enrichedProfile.Name = company.name ? company.name : '';
              enrichedProfile.Date_Founded__c = company.founded_on ? company.founded_on : '';
              enrichedProfile.No_of_Employees__c = company.num_employees_reported ? company.num_employees_reported : `${company.num_employees_min} - ${company.num_employees_max}`;
              enrichedProfile.Website = company.urls.homepage ? company.urls.homepage : '';
            }
            mappedCompanies.push(enrichedProfile);
          })
          .then(() => {
            if (res.data.data.length === mappedCompanies.length) {
              resolve(mappedCompanies);
            }
          });
      });
    })
    .catch((err) => {
      reject(err);
    });
});
