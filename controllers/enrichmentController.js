/* eslint-disable no-await-in-loop */
/* eslint-disable no-loop-func */
/* eslint-disable no-console */
/* eslint-disable linebreak-style */
/* eslint-disable no-else-return */

const sql = require('mssql');
const axios = require('axios');
const path = require('path');
const moment = require('moment');
const jsforce = require('jsforce');
const nodemailer = require('nodemailer');
require('events').EventEmitter.defaultMaxListeners = Infinity;

// Login with Marcus to the platform
const salesforceConnection = new jsforce.Connection({
  loginUrl: 'https://test.salesforce.com',
  username: 'salesforce.admin@ojassociates.com',
  password: 'ow3I3G6j',
});
const ENRICHMENTKEY = 'VTsZn8Iasy7fGgiR153LC2cl9OrK03QY1lhR7NrE';

const ovalConfig = {
  server: '10.210.2.6',
  requestTimeout: 700000,
  port: 1435,
  // If you're on Windows Azure, you will need this:
  options: {
    encrypt: true,
  },
  authentication: {
    type: 'default',
    options: {
      userName: 'EnrichmentAPI',
      password: '12Zybvws',

    },
  },
};
// login with my account to make the requests come from me.
salesforceConnection.login('salesforce.admin@ojassociates.com', 'ow3I3G6j', (err, userInfo) => {
  if (err) { return console.error(err); }
  // Now you can get the access token and instance URL information.
  // Save them to establish connection next time.
  console.log(`Logging into Salesforce on URL: ${salesforceConnection.instanceUrl}`);
  // logged in user property
  console.log(`User ID: ${userInfo.id}`);
  console.log(`Org ID: ${userInfo.organizationId}`);
});


const getCompanies = () => new Promise((resolve) => {
  sql.connect(ovalConfig)
    .then((conn) => new sql.Request())
    .then((request) => request.query('SELECT TOP(100) * FROM (SELECT *, ROW_NUMBER() OVER (PARTITION BY [URL] ORDER BY [AdaptNumber]) AS [RN] FROM [Staging].[dbo].[SalesforceClientEnrichement] WHERE IsURLValid = 1 ) x WHERE x.RN = 1 ORDER BY AdaptNumber'))
    .then((array) => {
      resolve(array.recordset);
    });
});


/*
    ! 1 WEEK
*/

const getWeeksUpdates = (offset, dateFrom, dateTo) => new Promise((resolve, reject) => {
  const currentDate = moment().toISOString();
  const oneWeekAgo = moment().subtract(1, 'week').toISOString();
  const url = `https://consumer-api.talentticker.ai/v1/companies?startDate=${dateFrom || oneWeekAgo}&endDate=${dateTo || currentDate}`;
  const headers = { headers: { 'x-api-key': ENRICHMENTKEY } };
  console.log(`${url}&limit=100&offset=${offset}`);
  const res = axios.get(`${url}&limit=100&offset=${offset}`, headers)
    .then((response) => response)
    .catch((err) => reject(err));
  resolve(res);
});

const getDomains = (recentlyUpdatedCompanies) => new Promise((resolve, reject) => {
  const domains = [];
  recentlyUpdatedCompanies.map((company, index) => {
    if (company.urls.homepage) {
      domains.push(company.urls.homepage.replace('www.', '').replace('https://', '').replace('http://', ''));
    }
    console.log(`Index: ${index}, RecentlyUpdatedCompanies: ${recentlyUpdatedCompanies.length}`);
    if (index + 1 === recentlyUpdatedCompanies.length) {
      console.log(domains.length);
      resolve(domains);
    }
  });
});

const getMatchedCompanies = (domains) => new Promise((resolve, reject) => {
  sql.close();
  sql.connect(ovalConfig)
    .then(() => new sql.Request())
    .then((request) => {
      console.log('EXEC');
      request.input('DomainList', sql.VarChar, domains);
      return request.execute('CDR_Salesforce.dbo.DomainToCompany_EnrichmentProfile');
    })
    .then((records) => {
      console.log('FINISHED SPROC');
      resolve(records.recordset);
    });
});

const pairCompanies = (enrichmentCompanies, salesforceCompanies) => new Promise((resolve, reject) => {
  console.log('STARTING PAIRING');

  const pairedCompanies = [];

  for (let index = 0; index < enrichmentCompanies.length; index++) {
    const talentTickerCompany = enrichmentCompanies[index];
    const pair = {
      talentTickerRecord: talentTickerCompany,
      relatedSalesforceRecords: [],
    };

    console.log('IN 1');
    for (let i = 0; i < salesforceCompanies.length; i++) {
      console.log('IN 2');
      console.log(`STARTING LENGTHS: S:${salesforceCompanies.length} | TT:${enrichmentCompanies.length}`);
      console.log(`TTINDEX: ${index}, SINDEX: ${i}`);

      const salesforceCompany = salesforceCompanies[i];
      const trimmedSalesforceDomain = salesforceCompany.Website.replace('www.', '').replace('https://', '').replace('http://', '');
      const trimmedTalentTickerDomain = talentTickerCompany.urls.homepage.replace('www.', '').replace('https://', '').replace('http://', '');

      if (salesforceCompany.API_Enrichment_External_ID__c) {
        if (salesforceCompany.API_Enrichment_External_ID__c === talentTickerCompany.id) {
          pair.talentTickerRecord = talentTickerCompany;
          pair.relatedSalesforceRecords.push(salesforceCompany);
        }
      } else if (trimmedTalentTickerDomain) {
        if (trimmedTalentTickerDomain === trimmedSalesforceDomain) {
          pair.talentTickerRecord = talentTickerCompany;
          pair.relatedSalesforceRecords.push(salesforceCompany);
        }
      }
      console.log(pairedCompanies.length);
      if (index + 1 === enrichmentCompanies.length) {
        console.log('FINISHED PAIRING');
        resolve(pairedCompanies);
      }
    }

    if (pair.relatedSalesforceRecords.length !== 0) {
      pairedCompanies.push(pair);
    }
  }
});

// ! DELETE FUNCTIONS ARE IN updateSingleSalesforceRecord() FUNCTION.
// ! DELETE THE KEYS YOU DO NOT WANT TO ADD TO SALESFORCE
// ! FOR EXAMPLE NOT REMOVING THE COMPANY WEBSITE WILL OVER_WRTIE SALESFORCE AS LONG AS THE KEY NAME IS THE SAME AS THE SALESFORCE KEY NAME.
// ! SEE DOCUMENTATION FOR A BETTER EXPLANATION. WITH PITCURES :O

const updateSingleSalesforceRecord = (pairedCompany) => new Promise((resolve, reject) => {
  const successfullUpdates = [];
  for (let index = 0; index < pairedCompany.relatedSalesforceRecords.length; index++) {
    const salesforceRecord = pairedCompany.relatedSalesforceRecords[index];
    const { talentTickerRecord } = pairedCompany;
    if (talentTickerRecord.founded_on) {
      salesforceRecord.Date_Founded__c = talentTickerRecord.founded_on;
    }

    if (talentTickerRecord.reviewed) {
      if (talentTickerRecord.num_employees_reported < 2) {
        salesforceRecord.NumberOfEmployees = 0;
      } else if (talentTickerRecord.num_employees_reported >= 2 && talentTickerRecord.num_employees_reported < 11) {
        salesforceRecord.NumberOfEmployees = 2;
      } else if (talentTickerRecord.num_employees_reported >= 11 && talentTickerRecord.num_employees_reported < 51) {
        salesforceRecord.NumberOfEmployees = 11;
      } else if (talentTickerRecord.num_employees_reported >= 51 && talentTickerRecord.num_employees_reported < 201) {
        salesforceRecord.NumberOfEmployees = 51;
      } else if (talentTickerRecord.num_employees_reported >= 201 && talentTickerRecord.num_employees_reported < 501) {
        salesforceRecord.NumberOfEmployees = 201;
      } else if (talentTickerRecord.num_employees_reported >= 501 && talentTickerRecord.num_employees_reported < 1001) {
        salesforceRecord.NumberOfEmployees = 501;
      } else if (talentTickerRecord.num_employees_reported >= 1001 && talentTickerRecord.num_employees_reported < 5001) {
        salesforceRecord.NumberOfEmployees = 1001;
      } else if (talentTickerRecord.num_employees_reported >= 5001 && talentTickerRecord.num_employees_reported < 10001) {
        salesforceRecord.NumberOfEmployees = 5001;
      } else if (talentTickerRecord.num_employees_reported >= 10001) {
        salesforceRecord.NumberOfEmployees = 10001;
      }
    } else {
      salesforceRecord.NumberOfEmployees = talentTickerRecord.num_employees_min;
    }

    delete salesforceRecord.LastModifiedDate;
    delete salesforceRecord.DomainID;
    delete salesforceRecord.Name;
    delete salesforceRecord.DomainName;
    delete salesforceRecord.MIGRATION_ID_COMPANY__c;

    successfullUpdates.push(salesforceRecord);
    console.log(index, pairedCompany.relatedSalesforceRecords.length);
    if (index + 1 === pairedCompany.relatedSalesforceRecords.length) {
      resolve(successfullUpdates);
    }
  }
});

const updateSalesforce = (salesforceRecords) => new Promise((resolve, reject) => {
  console.log('UPDATING SALESFORCE...');
  const job = salesforceConnection.bulk.createJob('Account', 'update');
  const batch = job.createBatch();
  console.log(salesforceRecords.length);
  batch.execute(salesforceRecords)
    .on('error', (batchInfo) => { // fired when batch request is queued in server.
      console.log('Error, batchInfo:', batchInfo);
      reject(batchInfo);
    })
    .on('response', (rets) => { // fired when batch is finished and result retrieved
      console.log('SALESFORCE UPDATED.');
      resolve(rets);
    });
});


const insertIntoSql = (companyPair, table) => new Promise((resolve, reject) => {
  for (let index = 0; index < companyPair.relatedSalesforceRecords.length; index++) {
    const salesforceRecord = companyPair.relatedSalesforceRecords[index];
    const { talentTickerRecord } = companyPair;
    sql.close();
    sql.connect(ovalConfig)
      .then(() => new sql.Request())
      .then((request) => {
        const valuesArray = [`'${salesforceRecord.Id}'`, `'${JSON.stringify(talentTickerRecord).toString().replace(/'/g, '\\"')}'`, `'${talentTickerRecord.id}'`];
        const headersArray = ['SalesforceID', 'JSON', 'talentTickerId'];

        if (salesforceRecord.Date_Founded__c) {
          headersArray.push('DateFounded');
          valuesArray.push(`'${salesforceRecord.Date_Founded__c}'`);
        }
        if (salesforceRecord.No_of_Employees__c) {
          headersArray.push('NoOfEmployees');
          valuesArray.push(`'${salesforceRecord.No_of_Employees__c}'`);
        }

        const statement = `INSERT INTO [${table}].[dbo].[CompanyEnrichmentRAW]`;
        const columnNames = `(${headersArray})`;
        const values = `VALUES(${valuesArray})`;

        const query = `${statement} ${columnNames} ${values}`;
        return request.query(query);
      })
      .then((res) => {
        resolve(salesforceRecord.Id);
      })
      .catch((err) => {
        reject(err);
      });
  }
});

const callSQLUpdateSproc = (table) => new Promise((resolve, reject) => {
  console.log('CALLING FINAL SPROC');
  sql.close();
  sql.connect(ovalConfig)
    .then(() => new sql.Request())
    .then((request) => request.query(`EXEC ${table}.dbo.UpdateCompanyEnrichmentRAW`))
    .then((res) => {
      if (res.rowsAffected[0] === 0) {
        resolve('UPDATE SUCCESSFUL');
      } else {
        reject('UPDATE UNSUCCESSFUL');
      }
    });
});


exports.enrichExsisting = async (req, res) => {
  let offset = 0;
  let status = 200;
  let recentlyUpdatedCompanies = [];
  let dateFrom = '';
  let dateTo = '';

  if (req.query.dateFrom && req.query.dateTo) {
    dateFrom = moment().format(req.query.dateFrom);
    dateTo = moment().format(req.query.dateTo);
  }

  if (req.query.dateFrom && !req.query.dateTo || !req.query.dateFrom && req.query.dateTo) {
    res.send('To use date range both is required.');
    return;
  }

  // Get Weekly updates
  do {
    try {
      const updatedCompanies = await getWeeksUpdates(offset, dateFrom, dateTo);
      status = updatedCompanies.status;
      recentlyUpdatedCompanies = recentlyUpdatedCompanies.concat(updatedCompanies.data.data);
      offset += 100;
    } catch (err) {
      status = 404;
    }
  } while (status !== 404);


  try {
    // Get a list of domains from the list of updated companies.
    const domainsList = await getDomains(recentlyUpdatedCompanies);


    // Get a list of companies that match via domain and salesforce/talentticker id
    const salesforceCompanies = await getMatchedCompanies(domainsList);


    // Pair them up easier to work with.
    // Structure
    //  const pair = {
    //  talentTickerRecord: talentTickerCompany,
    //  relatedSalesforceRecords: [],
    // };

    const pairedCompanies = await pairCompanies(recentlyUpdatedCompanies, salesforceCompanies);

    if (pairedCompanies.length === 0) {
      res.send('No Companies to Update');
    }


    let updatedPairedCompanies = [];
    let i = 0;
    do {
      for (let index = 0; index < pairedCompanies.length; index++) {
        i = index;
        const pairedCompany = pairedCompanies[index];
        const updatedCompany = await updateSingleSalesforceRecord(pairedCompany);
        updatedPairedCompanies = updatedPairedCompanies.concat(updatedCompany);
        console.log(`INDEX: ${index} | PAIREDCOMPANIES: ${pairedCompanies.length}`);
      }
    } while (i + 1 !== pairedCompanies.length);


    const insertedIntoSql = [];

    do {
      for (let index = 0; index < pairedCompanies.length; index++) {
        sql.close();
        const inserted = await insertIntoSql(pairedCompanies[index], req.query.table);
        insertedIntoSql.push(inserted);
      }
    } while (insertedIntoSql.length !== pairedCompanies.length);

    const updateSF = await updateSalesforce(updatedPairedCompanies);

    const updateSproc = await callSQLUpdateSproc(req.query.table);


    res.send({ message: `Succesfully updated ${insertedIntoSql.length} companies`, update: updateSproc, updatedIds: insertedIntoSql });
  } catch (err) {
    console.log(err);
    res.send(err);
  }
};

/*
    ! 24 HOURS
    ! Review Function Logic Duplication being filtered at the end.
*/

const getTodaysCompanies = async (amountOfDays = -1) => {
  const newCompanies = await sql.connect(ovalConfig)
    .then(() => new sql.Request())
    .then((request) => request.query(`SELECT No_of_Employees__c, Date_Founded__c, LastModifiedDate, Id, Website, Name  FROM [CDR_Salesforce].[dbo].[Company] WHERE LastModifiedDate >= DATEADD(DAY, ${amountOfDays} , GETDATE())`))
    //.then((request) => request.query(`SELECT top 500 No_of_Employees__c, Date_Founded__c, LastModifiedDate, Id, Website, Name FROM [CDR_Salesforce].[Salesforce].[Company] Order By LastModifiedDate DESC`))
    .then((response) => {
      sql.close();
      return response.recordset;
    });
  console.log(`${newCompanies.length} COMPANIES NEED UPDATING`);

  return newCompanies;
};

const callTalentTicker = (newCompanies) => new Promise((resolve, reject) => {
  console.log('CALLING TALENT TICKER ...');
  const responseList = [];
  const counter = 0;
  resolve(newCompanies.map(async (company) => {
    let url = '';
    const headers = { headers: { 'x-api-key': ENRICHMENTKEY } };
    console.log(company);
    if (company.Website) {
      url = `https://consumer-api.talentticker.ai/v1/companies?domain=${company.Website.replace('www.', '').replace('http://', '').replace('https://', '')}`;
    } else if (!company.Website && company.Name) {
      url = `https://consumer-api.talentticker.ai/v1/companies?name=${company.Name}`;
      console.log(`Name: ${company.Name}`);
    }

    const enrichedCompanies = await new Promise((innerResolve, innerReject) => innerResolve(axios.get(url, headers)
      .then((response, error) => {
        console.log('CALL');

        if (response.status === 200) {
          responseList.push(response.data.data[0]);


          if (counter === newCompanies.length) {
            console.log('RETURNING');
            console.log('TCL: callTalentTicker -> responseList', responseList.length);
            return responseList;
          }
        }
      })
      .catch((error) => {
        innerReject(error);
      })));


    return responseList;
  }));
});

const matchCompanies = (enrichmentCompanies, originalCompanies) => new Promise((resolve, reject) => {
  console.log('MATCHING COMPANIES');
  const pairs = [];
  for (let index = 0; index < originalCompanies.length; index++) {
    for (let i = 0; i < enrichmentCompanies.length; i++) {
      const pair = [];
      if (!originalCompanies[index].Website) {
        if (originalCompanies[index].Name.toString().toLowerCase().includes(enrichmentCompanies[i].name.toString().toLowerCase())) {
          pair.push(originalCompanies[index]);
          pair.push(enrichmentCompanies[i]);
          pairs.push(pair);
        }
      } else if (enrichmentCompanies[i].urls.homepage.toString().toLowerCase().includes(originalCompanies[index].Website.toString().toLowerCase())) {
        pair.push(originalCompanies[index]);
        pair.push(enrichmentCompanies[i]);
        pairs.push(pair);
      }

      if (index + 1 === originalCompanies.length) {
        resolve(pairs);
      }
    }
  }
});

const enrichCompanies = (pairedCompanies) => new Promise((resolve, reject) => {
  console.log('ENRICHING COMPANIES');
  const updatedCompaniesPairs = [];
  for (let index = 0; index < pairedCompanies.length; index++) {
    const pair = [];
    const salesforceRecord = pairedCompanies[index][0];
    const talentTickerRecord = pairedCompanies[index][1];


    if (!salesforceRecord.No_of_Employees__c) {
      if (talentTickerRecord.reviewed) {
        if (talentTickerRecord.num_employees_reported < 2) {
          salesforceRecord.No_of_Employees__c = 0;
        } else if (talentTickerRecord.num_employees_reported >= 2 && talentTickerRecord.num_employees_reported < 11) {
          salesforceRecord.No_of_Employees__c = 2;
        } else if (talentTickerRecord.num_employees_reported >= 11 && talentTickerRecord.num_employees_reported < 51) {
          salesforceRecord.No_of_Employees__c = 11;
        } else if (talentTickerRecord.num_employees_reported >= 51 && talentTickerRecord.num_employees_reported < 201) {
          salesforceRecord.No_of_Employees__c = 51;
        } else if (talentTickerRecord.num_employees_reported >= 201 && talentTickerRecord.num_employees_reported < 501) {
          salesforceRecord.No_of_Employees__c = 201;
        } else if (talentTickerRecord.num_employees_reported >= 501 && talentTickerRecord.num_employees_reported < 1001) {
          salesforceRecord.No_of_Employees__c = 501;
        } else if (talentTickerRecord.num_employees_reported >= 1001 && talentTickerRecord.num_employees_reported < 5001) {
          salesforceRecord.No_of_Employees__c = 1001;
        } else if (talentTickerRecord.num_employees_reported >= 5001 && talentTickerRecord.num_employees_reported < 10001) {
          salesforceRecord.No_of_Employees__c = 5001;
        } else if (talentTickerRecord.num_employees_reported >= 10001) {
          salesforceRecord.No_of_Employees__c = 10001;
        }
      } else {
        salesforceRecord.No_of_Employees__c = talentTickerRecord.num_employees_min;
      }
    }
    if (!salesforceRecord.Date_Founded__c) {
      if (talentTickerRecord.founded_on) {
        salesforceRecord.Date_Founded__c = talentTickerRecord.founded_on;
      }
    }

    pair.push(salesforceRecord);
    pair.push(talentTickerRecord);
    updatedCompaniesPairs.push(pair);

    if (updatedCompaniesPairs.length === pairedCompanies.length) {
      resolve(updatedCompaniesPairs);
    }
  }
});


const insertIntoOval = (updatedCompaniesPairs, table) => new Promise((resolve, reject) => {
  console.log('INSERTING INTO OVAL');
  for (let index = 0; index < updatedCompaniesPairs.length; index++) {
    sql.close();
    const salesforceRecord = updatedCompaniesPairs[index][0];
    const talentTickerRecord = updatedCompaniesPairs[index][1];
    sql.connect(ovalConfig)
      .then(() => new sql.Request())
      .then((request) => {
        const valuesArray = [`'${salesforceRecord.Id}'`, `'${JSON.stringify(talentTickerRecord).toString().replace(/'/g, '\\"')}'`, `'${talentTickerRecord.id}'`];
        const headersArray = ['SalesforceID', 'JSON', 'talentTickerId'];

        if (salesforceRecord.Date_Founded__c) {
          headersArray.push('DateFounded');
          valuesArray.push(`'${salesforceRecord.Date_Founded__c}'`);
        }
        if (salesforceRecord.No_of_Employees__c) {
          headersArray.push('NoOfEmployees');
          valuesArray.push(`'${salesforceRecord.No_of_Employees__c}'`);
        }

        const statement = `INSERT INTO [${table}].[dbo].[CompanyEnrichmentRAW]`;
        const columnNames = `(${headersArray})`;
        const values = `VALUES(${valuesArray})`;

        const query = `${statement} ${columnNames} ${values}`;
        return request.query(query);
      })
      .then((res) => {
        if (res) {
          resolve(res);
        }
      })
      .catch((err) => {
        reject(err);
      });
  }
});

// ! DELETE FUNCTIONS ARE IN updateSalesforceRecords() FUNCTION.
// ! DELETE THE KEYS YOU DO NOT WANT TO ADD TO SALESFORCE
// ! FOR EXAMPLE NOT REMOVING THE COMPANY WEBSITE WILL OVER_WRTIE SALESFORCE AS LONG AS THE KEY NAME IS THE SAME AS THE SALESFORCE KEY NAME.
// ! SEE DOCUMENTATION FOR A BETTER EXPLANATION. WITH PITCURES :O

const updateSalesforceRecords = (updatedCompanies) => new Promise((resolve, reject) => {
  console.log('UPDATING SALESFORCE RECORDS');
  const salesforceUpdated = [];
  for (let index = 0; index < updatedCompanies.length; index++) {
    const salesforceRecord = updatedCompanies[index][0];
    console.log('TCL: updateSalesforceRecords -> salesforceRecord', salesforceRecord);


    salesforceConnection.sobject('Account')
      .find({ Id: salesforceRecord.Id })
      .execute((err, record) => {
        console.log(salesforceRecord, 'SFRECORD');
        delete salesforceRecord.LastModifiedDate;
        delete salesforceRecord.Website;
        delete salesforceRecord.Name;
        salesforceConnection.sobject('Account').update([
          salesforceRecord,
        ], (error, ret) => {
          if (ret[0].success) {
            salesforceUpdated.push(salesforceRecord);
            if (updatedCompanies.length === salesforceUpdated.length) {
              resolve(true);
            }
            console.log(`Updated Successfully : ${salesforceRecord.Id}`);
          }
        });
      });
  }
});

const callUpdateSproc = (table) => new Promise((resolve, reject) => {
  console.log('CALLING FINAL SPROC');
  sql.close();
  sql.connect(ovalConfig)
    .then(() => new sql.Request())
    .then((request) => request.query(`EXEC ${table}.dbo.UpdateCompanyEnrichmentRAW`))
    .then((res) => {
      if (res.rowsAffected[0] === 0) {
        resolve('UPDATE SUCCESSFUL');
      } else {
        reject();
      }
    });
});


exports.enrichCreated = async (req, res) => {
  let amountOfDays;
  if (req.query.amountOfDays) {
    amountOfDays = req.query.amountOfDays;
  }

  // const tester4 = {
  //   No_of_Employees__c: null, Date_Founded__c: null, LastModifiedDate: Date.now(), Website: 'www.google.com', Name: 'Google', Id: '0012500001FhOWUAA3',
  // };

  // const tester5 = {
  //   No_of_Employees__c: null, Date_Founded__c: null, LastModifiedDate: Date.now(), Website: 'www.hsbc.com', Name: 'HSBC', Id: '0012500001FhQdFAAV',
  // };

  // const tester6 = {
  //   No_of_Employees__c: null, Date_Founded__c: null, LastModifiedDate: Date.now(), Website: 'www.natwest.com', Name: 'Natwest', Id: '0012500001FhQfHAAV',
  // };


  const newCompanies = await getTodaysCompanies(amountOfDays);
  // newCompanies.push(tester4);
  // newCompanies.push(tester5);
  // newCompanies.push(tester6);
  if (newCompanies.length === 0) {
    res.send('NO COMPANIES TO UPDATE.');
    return;
  }


  const enrichmentPromises = await callTalentTicker(newCompanies);

  let enrichmentCompanies = await Promise.all(enrichmentPromises).then((values) => values);

  // Horrible Solution. Needs refactoring each of the promises returns the array of all the results for some reason however, this does not do 9 calls to the API.
  enrichmentCompanies = enrichmentCompanies[0];

  const matchedCompanies = await matchCompanies(enrichmentCompanies, newCompanies);

  const updatedCompaniesPairs = await enrichCompanies(matchedCompanies);

  await insertIntoOval(updatedCompaniesPairs, req.query.table);


  await updateSalesforceRecords(updatedCompaniesPairs);


  const response = await callUpdateSproc(req.query.table);
  console.log('TCL: exports.enrichCreated -> response', response);

  if (response === 'UPDATE SUCCESSFUL') {
    res.status(200).json({ success: true, status: 200, message: 'UPDATE SUCCESSFUL' });
  }
};

/*
  !Bulk Update
*/


exports.enrichFast = async (req, res) => {
  const url = 'https://consumer-api.talentticker.ai/v1/companies';
  const headers = { headers: { 'x-api-key': 'VTsZn8Iasy7fGgiR153LC2cl9OrK03QY1lhR7NrE' } };
  const companies = await getCompanies();

  res.sendFile(path.join(`${__dirname}/index.html`));


  for (let index = 0; index < companies.length; index++) {
    let responseData;
    sql.close();
    // eslint-disable-next-line no-await-in-loop
    await new Promise((resolve, reject) => sql.connect(ovalConfig)
      .then(() => axios.get(`${url}?domain=${companies[index].URL.replace('https://', '').replace('http://', '').replace('www.', '').split('/')[0]}`, headers))
      .then((response) => {
        if (response.status === 200) {
          responseData = response;

          return new sql.Request();
        } else if (response.status === 202) {
          responseData = { data: { data: [{ id: '404', data: 'Could not find company' }] } };
          return new sql.Request();
        } else if (response.status === 429) {
          reject();
          res.send('NO MORE API REQUESTS LEFT.');
        }
      })
      .then((request) => {
        const valuesArray = [];
        const headersArray = ['adaptId', 'JSON', 'talentTickerId'];

        // Add constant Data

        valuesArray.push(companies[index].AdaptNumber);
        valuesArray.push(`'${JSON.stringify(responseData.data.data[0]).toString().replace(/'/g, '\\"')}}'`);
        valuesArray.push(`'${responseData.data.data[0].id}'`);

        // VALUES

        // Conditionally add values

        responseData.data.data[0].founded_on ? valuesArray.push(`'${responseData.data.data[0].founded_on}'`) : null;
        responseData.data.data[0].num_employees_reported ? valuesArray.push(`'${responseData.data.data[0].num_employees_reported}'`) : '';
        responseData.data.data[0].num_employees_min ? valuesArray.push(`'${responseData.data.data[0].num_employees_min}'`) : '';
        responseData.data.data[0].num_employees_max ? valuesArray.push(`'${responseData.data.data[0].num_employees_max}'`) : '';


        // HEADERS

        responseData.data.data[0].founded_on ? headersArray.push('DateFounded') : '';
        responseData.data.data[0].num_employees_reported ? headersArray.push('NoOfEmployees') : '';
        responseData.data.data[0].num_employees_min ? headersArray.push('NoOfEmployeesMin') : '';
        responseData.data.data[0].num_employees_max ? headersArray.push('NoOfEmployeesMax') : '';


        const statement = `INSERT INTO [${req.query.table}].[dbo].[CompanyEnrichmentRAW]`;
        const columnNames = `(${headersArray})`;
        const values = `VALUES(${valuesArray})`;

        const query = `${statement} ${columnNames} ${values}`;
        console.log(query);

        request.query(query);
      })
      .then(() => {
        setTimeout(() => {
          console.log(`INSERTED | ${responseData.status} | ${companies[index].URL.replace('https://', '').replace('http://', '').replace('www.', '')}`);
          resolve();
        }, 1000);
      }));
  }
};


exports.testConnection = (req, res) => {
  // Hello
  sql.close();
  sql.connect(ovalConfig)
    .then(() => new sql.Request())
    .then((request) => request.query('SELECT * FROM [Insights].[dbo].[Dim_Widget]'))
    .then((response) => {
      res.send(response.recordset);
    })
    .catch((err) => {
      res.status(500).send('MEMES');
    });
};


/*
  ! Test the scheduling
*/


const transporter = nodemailer.createTransport({
  service: 'outlook',
  auth: {
    user: 'it.support@ojassociates.com',
    pass: 'Atl455!an',
  },
});


exports.testWeekly = async (req, res) => {
  // EnrichmentFunctions.enrichExsisting();
  const mailOptions = {
    from: 'it.support@ojassociates.com',
    to: 'luke.brannagan@ojassociates.com',
    subject: '*** 🔔 ENRICHMENT API RUNNING - Weekly Load 🔔 ***',
    html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Weekly Load is Running</h1>',
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      throw error;
    } else {
      console.log('Email successfully sent!');
    }
  });


  axios({
    method: 'GET',
    url: 'localhost/Salesforce/enrichExsisting?table=OliverJames',
    proxy: {
      host: '127.0.0.1',
      port: 3000,
    },

  })
    .then((res, err) => {
      if (res) {
        const mailOptions1 = {
          from: 'it.support@ojassociates.com',
          to: 'luke.brannagan@ojassociates.com',
          subject: '*** ✅ Weekly Load - Has succeeded ✅ ***',
          html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Weekly Load has succeeded</h1>',
        };

        transporter.sendMail(mailOptions1, (error, info) => {
          if (error) {
            throw error;
          } else {
            console.log('Email successfully sent!');
          }
        });
      } else {
        const mailOptions2 = {
          from: 'it.support@ojassociates.com',
          to: 'luke.brannagan@ojassociates.com',
          subject: '*** 🚨 Weekly Load - has failed please investigate 🚨 ***',
          html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Weekly Load has failed</h1>',
        };

        transporter.sendMail(mailOptions2, (error, info) => {
          if (error) {
            throw error;
          } else {
            console.log('Email successfully sent!');
          }
        });
      }
    });

  console.log('running a task every minute');
};

exports.testNightly = async (req, res) => {
  const mailOptions = {
    from: 'it.support@ojassociates.com',
    to: 'luke.brannagan@ojassociates.com',
    subject: '*** 🔔 ENRICHMENT API RUNNING - Nightly Load 🔔 ***',
    html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Nightly Load is Running</h1>',
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      throw error;
    } else {
      console.log('Email successfully sent!');
    }
  });

  axios({
    method: 'GET',
    url: 'localhost/Salesforce/enrichCreated?table=SalesforceMigration&amountOfDays=-1',
    proxy: {
      host: '127.0.0.1',
      port: 3000,
    },
  })
    .then((res, err) => {
      if (res) {
        const mailOptions1 = {
          from: 'it.support@ojassociates.com',
          to: 'luke.brannagan@ojassociates.com',
          subject: '*** ✅ Nightly Load - Has succeeded ✅ ***',
          html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Nightly Load has succeeded</h1>',
        };

        transporter.sendMail(mailOptions1, (error, info) => {
          if (error) {
            throw error;
          } else {
            console.log('Email successfully sent!');
          }
        });
      } else {
        const mailOptions2 = {
          from: 'it.support@ojassociates.com',
          to: 'luke.brannagan@ojassociates.com',
          subject: '*** 🚨 Nightly Load - has failed please investigate 🚨 ***',
          html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Nightly Load has failed</h1>',
        };

        transporter.sendMail(mailOptions2, (error, info) => {
          if (error) {
            throw error;
          } else {
            console.log('Email successfully sent!');
          }
        });
      }
    });
};
