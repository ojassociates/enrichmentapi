
# OJE (Oliver James Enrichment API)

  ### TalentTicker Credentials
  ##### The Credentials for Talent Tickers api are: 
  Pre Release Key: **G0mwjvn6551jHFzC2oFAQ9JL0VpmOuik89Fx5AoT**
  Production Enrichement Key: **VTsZn8Iasy7fGgiR153LC2cl9OrK03QY1lhR7NrE**
  Production Events Key: **ibLRFCkNq416l2tXBoTJf9cLlHSLjXZI8pwe2CcF**

####  If TalentTicker ever changes their keys here are the locations to change: 

  Line 44
  
  

## /Salesforce

  

*  ### /enrichExsisting

* This is the endpoint to override the Weekly load run this is a weekly load has not ran or failed. (can also be used for trouble shooting.)

  

| Query Parameters| Description | Example | Required?|

|--|--|--|--|--|

| dateFrom | Date to search from. ONLY ISO 8601 DATE | 2019-10-25T14:12:43+0000 | NO | 

| dateTo | Date to search to. ONLY ISO 8601 DATE |2019-10-25T14:12:43+0000 |NO | 
| table | SQL table to connect to | SalesforceMigration | YES | 

  
  
  

*  ### /enrichCreated

* This is the endpoint to override the Nightly load run this is a weekly load has not ran or failed. (can also be used for trouble shooting.)

  

| Query Parameters| Description | Example | Required? | 

|--|--|--|--|--|

| amountOfDays| days to seach from or to | -100 (100 days ago)|NO | 
| table | SQL table to connect to | SalesforceMigration | YES | 

  
  
  

*  ### /testOval

* This endpoint is only to test that the server is running. You should see a table from the server you are connected to (if you change server you will need to change the table to select.)

  
  

## Config (Scheduling)

  

### Scheduling

The Config and the scheduling is setup in the app.js.

  

The npm package that has been used is available here: [https://www.npmjs.com/package/node-cron](https://www.npmjs.com/package/node-cron)

* To setup the cron the first 5 parameters are time scales to set to. Guide is on the docs link above.

  

### Auto Emails

  

Small and quick done with node-mailer for ease and is sent from the IT Support account.

  

## Code

  
### What is the API actually doing/ Overview

This is an enrichment API in short it updates Salesforce on a nightly and weekly basis. 

####  Nightly
Each night at 23:50 we run the nightly load which checks Salesforce.Company if any companies in Salesforce have been created if this is true then we update the records with the founded date of the company as well as the amount of employees at the company. 

#### Weekly
Each week on a Saturday at 23:50 we run the weekly load which checks Talent Ticker to see what has been updated (using the date range selectors from their API). If they have some updates that match any of the companies in Salesforce we will update them. 

### The use of async
You're going to need to read this:
[https://dev.to/siwalikm/async-programming-basics-every-js-developer-should-know-in-2018-a9c](https://dev.to/siwalikm/async-programming-basics-every-js-developer-should-know-in-2018-a9c)


 ### How to add a new field/remove (for logic).
 Adding a new field can be done in a few small steps: 
##### Nightly 
1. Modify the select query in **getTodaysCompanies** (Line 370)
2. Then edit the **enrichCompanies** (Line 447) to see if any ammendments need to be made to the Salesforce record. 
3.  ***Optional*** - insert the extra data in insertIntoOval (Line 486)
4.  **Line 546** includes a delete statement to remove keys/values from an Object, this will stop it being mapped to its counterpart in Salesforce or leave it in and update the field. 


##### Weekly
 1. Modify the sproc **[dbo].[DomainToCompany_EnrichmentProfile]** (**Alex Dean** is the man).
 2.  Delete statements are in updateSingleSalesforceRecord (Line 154) include them if you do not want to update salesforce with this record. Leave them in if you want to update them. 
 3.  ***Optional*** - insert the extra data in insertIntoSql(Line 215)

### How to change connection to Tank.

Below is the login for the connection string to the SQL server you will be using. 
It is in the enrichmentController.js 

You will also need to change the authentication and create a login on Tank or use an exsisting account.

    const  ovalConfig  =  {
    server:  '10.210.2.6',
    requestTimeout:  700000,
    port:  1435,// If you're on Windows Azure, you will need this:
    options:  {
    encrypt:  true,
    },
    authentication:  {
    type:  'default',
    options:  {
    userName:  'EnrichmentAPI',
    password:  '12Zybvws',
			  }
		,}
    ,};
	
### Logins with Salesforce 
Change this to any SF account: 

    const  salesforceConnection  =  new  jsforce.Connection({
    loginUrl:'https://test.salesforce.com',
    username:'Marcus.Baxendell@ojassociates.com',
    password:  'Password9!joUBm4tVW3zcSJ51ZwIiiCy1t',
    });
    
   

### How to setup the auto emails.
 Emails are created with nodemailer. We login first we are currently logged into the IT Support email in the code with the variable named transported at the top of the code in app.js. 

Here are the docs for nodemailer its pretty easy:
[https://nodemailer.com/about/](https://nodemailer.com/about/)


