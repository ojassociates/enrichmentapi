const express = require('express');
const router = express.Router();

const talentTickerEndpoint = 'https://consumer-api.talentticker.ai/v1/companies';
const talentTickerHeaders = { headers: { 'X-API-KEY': 'VTsZn8Iasy7fGgiR153LC2cl9OrK03QY1lhR7NrE' } };
const Enrichment = require('../controllers/enrichmentController.js');

router.get('/enrichExsisting', Enrichment.enrichExsisting);
router.get('/enrichCreated', Enrichment.enrichCreated);
router.get('/testOval', Enrichment.testConnection);
router.get('/updateFast', Enrichment.enrichFast);
router.get('/testNightly', Enrichment.testNightly);
router.get('/testWeekly', Enrichment.testWeekly);



module.exports = router;
