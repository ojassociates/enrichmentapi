
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cron = require('node-cron');
const nodemailer = require('nodemailer');
const axios = require('axios');

const timeout = require('connect-timeout'); // express v4

const emailReceiver = 'Development.TeamDevelopment@ojassociates.com';

const transporter = nodemailer.createTransport({
  service: 'outlook',
  auth: {
    user: 'it.support@ojassociates.com',
    pass: 'Atl455!an',
  },
});

// Notification when the code is running.
// const mailOptions = {
//   from: 'it.support@ojassociates.com',
//   to: 'luke.brannagan@ojassociates.com',
//   subject: '*** ENRICHMENT API RUNNING ***',
//   html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">The API is Running</h1>',
// };

// transporter.sendMail(mailOptions, (error, info) => {
//   if (error) {
//     throw error;
//   } else {
//     console.log('Email successfully sent!');
//   }
// });

const EnrichmentFunctions = require('./controllers/enrichmentController');

const enrichementRouter = require('./routes/enrichment');

const app = express();

app.use(timeout('21476s'));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/Salesforce', enrichementRouter);


// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// SCHEDULING

// WEEKLY
cron.schedule('30 18 * * Friday', () => {
  // EnrichmentFunctions.enrichExsisting();
  const mailOptions = {
    from: 'it.support@ojassociates.com',
    to: emailReceiver,
    subject: '*** 🔔 ENRICHMENT API RUNNING - Weekly Load 🔔 ***',
    html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Weekly Load is Running</h1>',
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      throw error;
    } else {
      console.log('Email successfully sent!');
    }
  });

  axios.get('https://oja-enrichmentapi.azurewebsites.net/Salesforce/enrichExsisting?table=OliverJames')
    .then((res, err) => {
      if (res) {
        const mailOptions1 = {
          from: 'it.support@ojassociates.com',
          to: emailReceiver,
          subject: '*** ✅ Weekly Load - Has succeeded ✅ ***',
          html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Weekly Load has succeeded</h1>',
        };

        transporter.sendMail(mailOptions1, (error, info) => {
          if (error) {
            throw error;
          } else {
            console.log('Email successfully sent!');
          }
        });
      } else {
        const mailOptions2 = {
          from: 'it.support@ojassociates.com',
          to: emailReceiver,
          subject: '*** 🚨 Weekly Load - has failed please investigate 🚨 ***',
          html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Weekly Load has failed</h1>',
        };

        transporter.sendMail(mailOptions2, (error, info) => {
          if (error) {
            throw error;
          } else {
            console.log('Email successfully sent!');
          }
        });
      }
    });

  console.log('running a task every minute');
});


// NIGHTLY
cron.schedule('15 15 * * Monday,Tuesday,Wednesday,Thursday', () => {
  const mailOptions = {
    from: 'it.support@ojassociates.com',
    to: emailReceiver,
    subject: '*** 🔔 ENRICHMENT API RUNNING - Nightly Load 🔔 ***',
    html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Nightly Load is Running</h1>',
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      throw error;
    } else {
      console.log('Email successfully sent!');
    }
  });

  axios.get('https://oja-enrichmentapi.azurewebsites.net/Salesforce/enrichCreated?table=SalesforceMigration&amountOfDays=-1')
    .then((res, err) => {
      if (res) {
        const mailOptions1 = {
          from: 'it.support@ojassociates.com',
          to: emailReceiver,
          subject: '*** ✅ Nightly Load - Has succeeded ✅ ***',
          html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Nightly Load has succeeded</h1>',
        };

        transporter.sendMail(mailOptions1, (error, info) => {
          if (error) {
            throw error;
          } else {
            console.log('Email successfully sent!');
          }
        });
      } else {
        const mailOptions2 = {
          from: 'it.support@ojassociates.com',
          to: emailReceiver,
          subject: '*** 🚨 Nightly Load - has failed please investigate 🚨 ***',
          html: '<h1 style="background-image: linear-gradient(to left, violet, indigo, blue, green, yellow, orange, red); -webkit-background-clip: text; -webkit-text-fill-color: transparent;">Nightly Load has failed</h1>',
        };

        transporter.sendMail(mailOptions2, (error, info) => {
          if (error) {
            throw error;
          } else {
            console.log('Email successfully sent!');
          }
        });
      }
    });
});

module.exports = app;
